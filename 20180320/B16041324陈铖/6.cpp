#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;
int judge(int x)
{
 	int i,temp=1;
 	int m = sqrt(x);
 	if(x==2)
 	temp=1;
 	else if(x>2){
 	for(i=2;i<=m;i++){
 		if(x%i==0)
 		temp=0;
 		}
 	}	
	return temp;
}

int main()
{
	int n=0,i;
 	for(i=2;i<=1000;i++){
		if(judge(i)){
			cout<<setw(6)<<i;
 			n++;
 			if(n%5!=0)
 				continue;
 			cout<<endl;	
 		} 
 	}
 	return 0;
}
