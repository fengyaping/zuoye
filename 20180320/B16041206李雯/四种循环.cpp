#include<iostream>
using namespace std;
int F_for(int n)
{
	int sum = 0;
	for (int i = 1;i <= n;i++)
	{
		int j = i;
		for (;j >= 1;)
		{
			sum = sum + j % 10;
			j /= 10;
		}
	}
	return sum;
}
int S_while(int n)
{
	int sum = 0;
	int i = 1;
	while (i <= n)
	{
		int j = i;
		while (j >= 1)
		{
			sum = sum + j % 10;
			j /= 10;
		}
		i++;
	}
	return sum;
}
int T_do_while(int n)
{
	int sum = 0;
	int i = 1;
	do
	{
		int j = i;
		do
		{
			sum = sum + j % 10;
			j /= 10;
		} while (j > 0);
		i++;
	} while (i <= n);
	return sum;
} 
int F_while1(int n)
{
	int sum = 0;
	int i = 1;
	while (1)
	{
		int j = i;
		while (1)
		{
			if (j > 0) 
			{
				sum = sum + j % 10;
				j /= 10;
			}
			else
			{
				break;
			}
		}
		if (i == n)
		{
			break;
		}
		i++;		
	}
	return sum;
}
int main()
{
	int num;
	cin>>num;
	cout<<F_for(num)<<" "<<S_while(num)<<" "<<T_do_while(num)<<" "<<F_while1(num);
    return 0;
}

