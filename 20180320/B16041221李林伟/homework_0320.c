#include<stdio.h>
#include<stdbool.h>
#define size 1001

int sum1(int n){
	int sum = 0;
	while(n != 0){
		sum += n % 10;
		n /= 10;
	}
	return sum;
}

int sum2(int n){
	int sum = 0;
	for(; n != 0; n /= 10){
		sum += n % 10;
	}
	return sum;
}

int sum3(int n){
	int sum = 0;
	while(1){
		sum += n % 10;
		n /= 10;
		if(n == 0) break; 
	}
	return sum;
}

int sum4(int n){
	int sum = 0;
	do{
		sum += n % 10;
		n /= 10;
	}while(n != 0);
	return sum;
}

void init_flag(bool flag[]){  // 初始化bool数组  
	for(int i = 0; i < size; i++)
		flag[i] = true;
}

void eratosthenes(bool flag[]){  // 埃式筛法 
	flag[0] = flag[1] = false;  // 0，1，不是素数
	for(int i = 2; i < size/2+1; i++){  // 缩小边界，尽量减少循环次数 
		if(flag[i])
			for(int j = 2*i; j < size; j += i)
				flag[j] = 0;
	} 
}

int main(){
	int x = 1141;
	printf("question1: %d %d %d %d\n", sum1(x), sum2(x), sum3(x), sum4(x));
	bool flag[size];  // -std=c99  初始一个标志数组 
	init_flag(flag);  // 初始置true 
	eratosthenes(flag);
	int sign = 0;
	printf("question2: \n");
	for(int i = 0; i < size; i++)
		if(flag[i]){
			printf("%5d", i);
			sign++;
			if(sign % 5)
				continue; // 应题目要求 使用 continue 
			printf("\n");		
		}
	return 0;
}

