#include<iostream>
#include<iomanip>
using namespace std;
void matrix()
{
	int i,j;
	int a[7][7];
	a[0][0]=1;
	for(i=1;i<7;i++)
	{
		a[i][0]=a[i-1][i-1]+1;
		for(j=1;j<=i;j++)
		{
			a[i][j]=a[i][j-1]+1;
		}
	}
	for(j=0;j<7;j++)
	{
		for(i=0;i<j;i++)
		{
			a[i][j]=a[j][i];
		}
	}
	for(i=0;i<7;i++)
	{
		for(j=0;j<7;j++)
		{
			cout<<setw(4)<<a[i][j];
		}
		cout<<endl;
	}
}
int main()
{
	matrix();
	return 0;
}
