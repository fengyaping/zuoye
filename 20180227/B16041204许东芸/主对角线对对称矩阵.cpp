#include<iostream>
#include<ctime>
using namespace std;
int main()
{
	int n=4;
	int m=4;
	int a[m][n];
int i,j;
int v=1;
 
 for(i=0;i<n;i++)      //以四行四列的完整形式输出主对角线对称矩阵 
 {
   for(j=0;j<n;j++)
   {
   	 printf("%2d ",a[i][j]);
   }
   printf("\n");
}
for(i=0;i<n;i++)    //以下三角的形式存入矩阵 (只有一半) 
 {
   for(j=0;j<=i;j++)
   {
   a[i][j]=v;
   v++;
   a[j][i]=a[i][j];
 //  printf("%2d ",a[i][j]);
   }
   //printf("\n");
 }
return 0;
}
//总结：输出下三角时，存在一个完整的矩阵，只是上面三角部分为0，使用 a[j][i]=a[i][j];将上面部分赋值以后，形成完整的主对角线矩阵，
//又通过嵌套的两个for循环，将对称完的数字输出，因此，第二次的for循环，只起到输出矩阵的作用，不进行赋值。 
 
