#include <stdio.h>
#include <assert.h>

char *strcpy(char *strDest, const char *strScr)
{
       char *address = strDest;
       assert((strDest != NULL) && (strScr != NULL));
       while(*strScr)
       {
              *strDest++ = *strScr++;
       }
       *strDest = '\0';
       return address;
}
int main()
{
       char str1[]={"Hello"};
       char str2[]={"World��"};
       printf("%s\n",strcpy(str1,str2));
       return 0;
}
