#include<stdio.h>
#include<iostream>
using namespace std;
int main()
{
	int a[4][4] = { 1,2,3,4,9,10,11,12,13,9,5,1,15,11,7,3 }, b[4][4] = {}, c[4][4] = {};
	int i, j;
	cout << "原矩阵：\n";
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			cout << a[i][j]<<" ";
		}
		cout << "\n";
	}
	cout << "顺时针旋转后：\n";
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			b[i][j] = a[3-j][i];
			cout << b[i][j]<<" ";
		}
		cout << "\n";
	}
	cout << "逆时针旋转后：\n";
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			c[i][j] = a[j][3-i];
			cout << c[i][j] << " ";
		}
		cout << "\n";
	}
	system("pause");
	return 0;
}