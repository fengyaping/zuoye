#include <iostream>
#include <cstring>
using namespace std;
void Transform(char *a)
{
	int len=strlen(a);
   for(int j=0;j<len/2;j++)
   {
      char ch=a[j];
	  a[j]=a[len-j-1];
	  a[len-j-1]=ch;
   }
	int i=0;
   while(a[i]!='\0')
   {
	   if(a[i]>='a'&&a[i]<='z')
	   {
		    a[i]=(a[i]+3-'a'+26)%26+'a';
	        a[i]=a[i]-32;
	   }
	   else if(a[i]>='A'&&a[i]<='Z')
	   {
		     a[i]=(a[i]+3-'A'+26)%26+'A';
		    a[i]=a[i]+32;
	   }
       i++;
   }
}
int main()
{
  cout<<"请输入一个等待解密的字符串:"<<endl;
  char a[100]="";
  gets(a);
  Transform(a);
  puts(a);
  
  return 0;
}