#include <stdio.h>
int main()
{
    char in = getchar();
    while (in != '\n')
    {
        in = (in + 2 - 'a') % 26 + 'a';
        putchar(in);
        in = getchar();
    }
    return 0;
}
