//
//  main.c
//  0327.c
//
//  Created by 黄靖文 on 2018/4/9.
//  Copyright © 2018年 黄靖文. All rights reserved.
//

#include <stdio.h>

int main()
{
    int i;
    char s[100];
    gets(s);
    for (i = 0;s[i] != '\0';i++)
    {
        if (s[i] >= '0'&&s[i] <= '9')
            s[i] = (s[i] + 2 - '0') % 10 + '0';
        else if (s[i] >= 'a'&&s[i] <= 'z')
            s[i] = (s[i] + 2 - 'a') % 26 + 'a';
        else if (s[i] >= 'A'&&s[i] <= 'Z')
            s[i] = (s[i] + 2 - 'A') % 26 + 'A';
    }
    puts(s);
    return 0;
}
