#include<iostream>
using namespace std;
int** rotation(int array[][4] ){
	//将矩阵顺时针旋转90度 
	int **temp=new int *[4];
	for(int i=0;i<4;i++){
		temp[i]=new int[4];
	}
	for(int i=0;i<4;i++){
	
		for(int j=0;j<4;j++){
			temp[j][3-i]=array[i][j];
		}
	}
	
	return temp;
}
int** reverseRotation(int array[][4] ){
	//将矩阵顺时针旋转90度 
	int **temp=new int *[4];
	for(int i=0;i<4;i++){
		temp[i]=new int[4];
	}
	for(int i=0;i<4;i++){
	
		for(int j=0;j<4;j++){
			temp[3-j][i]=array[i][j];
		}
	}
	return temp;
}
int main(){
	int array[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
        printf("------旋转前-----\n");
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
		     printf("--%d--",array[i][j]);
		}
		printf("\n");
	}
	printf("------顺时针旋转后-----\n");
	int **newarr=rotation(array);
		for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
		     printf("--%d--",newarr[i][j]);
		}
		printf("\n");
	}
		printf("------逆时针旋转后-----\n");
	 newarr=reverseRotation(array);
		for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
		     printf("--%d--",newarr[i][j]);
		}
		printf("\n");
	}
}
